= Using dnf

The {projname} default container image includes https://github.com/rpm-software-management/dnf[dnf].

== Using `dnf` as part of a container build

A key toplevel goal of this project is to support and encourage creating
custom derived container images, using every tool and technique that applies
to "application" containers.

Many examples use `RUN dnf install`.  There is nothing special about the use
of `dnf` in a bootc-derived container versus an application container; it
is exactly the same code.

== Using `dnf` at runtime

The system is mounted read-only when "deployed" as a physical or virtual machine.  

At the current time, depending on the version, `dnf install` for
example may appear to start working, but will error out with e.g.

`error: can't create transaction lock on /usr/share/rpm/.rpm.lock (Read-only file system)`

This is a known bug; see e.g. https://github.com/rpm-software-management/dnf/pull/2053/[this PR].

=== Enabling "transient" package installations

Using the `bootc usroverlay` command, a transient writable overlay filesystem
is created for `/usr`, and `dnf install` will write to that overlay.  This is
a useful pattern for installing debugging tools but changes will be lost on reboot.

== rpm-ostree

At the current time, `rpm-ostree` is also included in the base image, though
this may change in a future version. For more, see xref:rpm-ostree.adoc[rpm-ostree].

== Deploying `dnf` based images

Conceptually, it would be possible to extend the tooling that surrounds
`fedora-bootc` to also enable a model where this would work:

```
FROM quay.io/fedora/fedora:41
RUN dnf -y install kernel
```

And then pass that to e.g. Anaconda to write to disk.

In this theoretical model, there wouldn't be `bootc`
in the deployed image, and updates could be done by
`dnf` directly.

The core disadvantages of this would be:

- Losing transactional updates and default runtime immutability
- Losing support for updating non-RPM content

At the current time such a model is not within the direct
focus of this project.

However, technically this would only be a small
extension of the existing Anaconda support for deploying
a raw tarball, and if someone was interested in working
on it, it could be added to the overall Fedora/CentOS
container-related project scope.

