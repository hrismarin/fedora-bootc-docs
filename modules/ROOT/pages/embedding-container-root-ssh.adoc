:page-partial:

See https://gitlab.com/bootc-org/examples/-/tree/main/included-ssh-pubkey[examples/included-ssh-pubkey]
for the canonical reference. Here is a copy for convenience:

[source]
----
FROM <baseimage>
# You *must* specify this argument; it is a SSH public key
# (in general an authorized_keys formatted data)
# that will be used for the `root` user by default.
ARG SSHPUBKEY
# In this example, we add /usr/ssh to the search path OpenSSH uses for keys.
# The rationale for this is that `/usr` is always part of the immutable
# container state, as opposed to user home directories which are mutable.
# In this pattern, you can always have a "fallback" key available, but
# e.g. use an external system (such as cloud-init) to live-update
# the traditional authorized_keys in the user's home directories.
RUN set -eu; mkdir -p /usr/ssh && \
    echo 'AuthorizedKeysFile /usr/ssh/%u.keys .ssh/authorized_keys .ssh/authorized_keys2' >> /etc/ssh/sshd_config.d/30-auth-system.conf && \
    echo ${SSHPUBKEY} > /usr/ssh/root.keys && chmod 0600 /usr/ssh/root.keys
----

Provide your public SSH key as a build argument:

[source]
----
$ podman build --build-arg=SSHPUBKEY=$HOME/.ssh/id_rsa.pub .
----
