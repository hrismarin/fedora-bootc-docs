
= Air-gapped and disconnected updates

For environments without a direct connection to a centralized container
registry, we encourage mirroring an on-premise registry if possible or manually
moving container images using `skopeo copy`.
See https://www.redhat.com/sysadmin/manage-container-registries[this blog]
for example.

For systems that require manual updates via USB drives, this procedure
describes how to use `skopeo` and `bootc switch`.

Copy image to USB Drive:

`skopeo copy docker://[registry]/[path to image] dir://run/media/$USER/$DRIVE/$DIR`

You may also use the https://github.com/containers/skopeo/blob/main/docs/skopeo-sync.1.md[skopeo sync] command to synchronize entire registries or namespaces.

**NOTE** Using the dir transport will create a number of files,
and it's recommended to place the image in it's own directory.
If the image is local the containers-storage transport will transfer
the image from a system directly to the drive:

`skopeo copy containers-storage:[image]:[tag] dir://run/media/$USER/$DRIVE/$DIR`

From the client system, insert the USB drive and mount it:

`mount /dev/$DRIVE /mnt`

`bootc switch` will direct the system to look at this mount point for future
updates, and is only necessary to run one time if you wish to continue
consuming updates from USB devices. note that if the mount point changes,
simply run this command to point to the alternate location. We recommend
using the same location each time to simplfy this.

`bootc switch --transport dir /mnt/$DIR`

Finally `bootc upgrade` will check for updates and reboot the system (when `--apply` is used).

`bootc upgrade --apply`
